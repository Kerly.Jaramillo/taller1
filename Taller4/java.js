/*1.ejercicio1*/

function inicio1() {
    let octal = document.getElementById("entrada1").value;
    alert("El número octal " + octal + " en decimal es: " + parseInt(octal, 8));
  }
  
/*Ejercicio 2 */
function inicio2() {
    let num1 = parseInt(document.getElementById("entrada2").value);
    let num2 = parseInt(document.getElementById("entrada3").value);
    alert(Calculator(num1, num2));
  }
  function Calculator(num1, num2) {
    return (
      "La suma de " + num1 + " + " + num2 +" es: " +(num1 + num2) + 
      "\nLa resta de " + num1 + " - " + num2 + " es: " + (num1 - num2) +
      "\nLa multiplicación de " + num1 + " * " + num2 + " es: " + (num1 * num2) +
      "\nLa división de " + num1 + " / " + num2 + " es: " + (num1 / num2).toFixed(2)
    );
  }
  