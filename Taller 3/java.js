//ejercicio #1
function start1() {
    let date = new Date();
    document.getElementById("Numero1").value =
      date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    document.getElementById("exit1").value =
      date.getHours() * 3600 +
      date.getMinutes() * 60 +
      date.getSeconds() +
      " segundos";
  }
  
  //ejercicio #2
  function start2() {
    let base = document.getElementById("numero2").value;
    let altura = document.getElementById("numero2-1").value;
    document.getElementById("exit2").value =
      "El área de un triángulo es igual a: " +
      (base * altura) / 2;
  }
  
  //ejercicio #3
  function start3() {
    let num = document.getElementById("numero3").value;
    let raiz = Math.sqrt(num);
    let result;
    if (raiz < 10) {
      result = raiz.toFixed(2);
    } else {
      if (raiz < 100) {
        result = raiz.toFixed(1);
      } else {
        result = raiz.toFixed(0);
      }
    }
    document.getElementById("numero3").value =
      "La raíz cuadrada es: " + result;
  }
  
  // ejercicio #4
  function start4() {
    let cadena = document.getElementById("numero4").value;
    document.getElementById("exit4").value =
      "La longitud es de: " +
      cadena.length +
      " caracteres";
  }
  
  // ejercicio #5
  let array1 = ["Lunes", " Martes", " Miércoles", " Jueves", " Viernes"];
  let array2 = [" Sábado", " Domingo"];
  document.getElementById("numero5").value = array1;
  document.getElementById("numero5-1").value = array2;
  
  function start5() {
    document.getElementById("exit5").value =
      "La concatenación es"  +
      array1.concat(array2) +
      array1.concat(array2).length;
  }
  
  // ejercicio #6
  function start6() {
    document.getElementById("exit6").value = navigator.appVersion;
  }
  // ejercicio #7
  function start7() {
    document.getElementById("exit7").value =
      "La medida de tu pantalla es de: " +
      screen.width +
      " x " +
      screen.height;
  }
  // ejercicio #8
  function start8() {
    let validate = confirm("¿Desea imprimir esta página web?");
    if (validate) {
      window.print();
    }
  }
  